<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>

<p>
<a href="Rindex.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Back</a>
</p>

<table id="ghatable" class="display table table-bordered table-stripe" cellspacing="0" width="100%">

<thead>
     <tr>
          <th>Name</th>
          <th>Ordered_date</th>
     </tr>
</thead>

<?php
$res = $mysqli->query('SELECT p.product_name, i.ordered_date  FROM product as p INNER JOIN list_item as i ON p.id=i.product_id ORDER BY i.ordered_date');
while ($row = $res->fetch_assoc()):
?>
     <tr>
          <td><?php echo $row['product_name']; ?></td>
          <td><?php echo $row['ordered_date']; ?></td>
     </tr>
<?php
endwhile;
?>
</tbody>
</table>
<?php
include '../Assets/footer.php';

?>