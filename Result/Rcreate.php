<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>
<a href="Rindex.php" class="btn btn-success btn-md"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</a>

<?php
if (isset($_POST['bts'])):
    if ($_POST['pid'] != null && $_POST['pn'] != null && $_POST['des'] != null) {
        $stmt = $mysqli->prepare('INSERT INTO product (id,product_name,product_description) VALUES (?,?,?)');
        $stmt->bind_param('iss', $pid, $pn, $des);

        $pid = $_POST['pid'];
        $pn = $_POST['pn'];
        $des = $_POST['des'];

        if ($stmt->execute()):
?>
				<p></p>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				  <strong>Created!</strong> Data successfully created!! <a href="Oindex.php">Home</a>.
				</div>
<?php
else:
?>
<p></p>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <strong>Error!</strong> Inserting data!!!.<?php echo $stmt->error; ?>
</div>
<?php
endif;
    } else {
        ?>
<p></p>
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <strong>Successfull!</strong> Data created successfully.
</div>
<?php
    }
endif;
?>

<p>
</p>
     <div class="panel panel-default">
       <div class="panel-body">

  <form role="form" method="post">
  <div class="form-group">
      <label for="pid">Product Id</label>
      <input type="text" class="form-control" name="pid" id="pid" placeholder="Product Id">
    </div>

   <div class="form-group">
      <label for="pn">Product Name</label>
      <input type="text" class="form-control" name="pn" id="pn" placeholder="Product Name">
    </div>

    <div class="form-group">
      <label for="des">Description</label>
      <textarea class="form-control" name="des" id="des" rows="3"></textarea>
    </div>
    <button type="submit" name="bts" class="btn btn-default">Submit</button>
  </form>
<?php
include '../Assets/footer.php';
?>