<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>
<p>
<a href="Rcreate.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Data</a>
<a href="Rproductlist.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Product list By Name,Date</a>

</p>
<table id="ghatable" class="display table table-bordered table-stripe" cellspacing="0" width="100%">
<thead>
     <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Description</th>
          <th>created_date</th>
          <th>updated_date</th>
     </tr>
</thead>
<tbody>
<?php

$res = $mysqli->query('SELECT * FROM product');
while ($row = $res->fetch_assoc()):
?>
     <tr>
          <td><?php echo $row['id']; ?></td>
          <td><?php echo $row['product_name']; ?></td>
          <td><?php echo $row['product_description']; ?></td>
          <td><?php echo $row['created_date']; ?></td>
          <td><?php echo $row['modified_date']; ?></td>
          <td>
          <a href="Rupdate.php?u=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
          <a onclick="return confirm('Are you sure u want to delete data')" href="Rdelete.php?d=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
          </td>
     </tr>
<?php
endwhile;
?>
</tbody>
</table>
<?php
include '../Assets/footer.php';

?>