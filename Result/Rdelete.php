<?php

include '../Assets/config.php';
if (isset($_GET['d'])):
    $stmt = $mysqli->prepare('DELETE FROM product WHERE id=?');
    $stmt->bind_param('i', $id);

    $id = $_GET['d'];

    if ($stmt->execute()):
        echo "<script>location.href='Rindex.php'</script>";
    else:
        echo "<script>alert('".$stmt->error."')</script>";
    endif;
endif;
