<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>
<p>
<a href="Ocreate.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Data</a>
<a href="customerOrder.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Customers with Orders</a>
</p>

<table id="ghatable" class="display table table-bordered table-stripe" cellspacing="0" width="100%">
<thead>
     <tr>
          <th>Id</th>
          <th>Status</th>
          <th>Order Code</th>
          <th>Action</th>
     </tr>
</thead>
<tbody>
<?php

$res = $mysqli->query('SELECT * FROM orders');
while ($row = $res->fetch_assoc()):
?>
     <tr>
          <td><?php echo $row['id']; ?></td>
          <td><?php echo $row['status']; ?></td>
          <td><?php echo $row['order_code']; ?></td>
          <td>
          <a href="Oupdate.php?u=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
          <a onclick="return confirm('Are you sure u want to delete data')" href="Odelete.php?d=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
          </td>
     </tr>
<?php
endwhile;
?>
</tbody>
</table>
<?php
include '../Assets/footer.php';

?>