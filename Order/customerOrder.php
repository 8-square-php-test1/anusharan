<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>
<p>
<a href="Ocreate.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Data</a>
<a href="customerOrder.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Customers with Orders</a>
</p>

<table id="ghatable" class="display table table-bordered table-stripe" cellspacing="0" width="100%">
<thead>
     <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Address</th>
          <th>Status</th>
     </tr>
</thead>
<tbody>

<?php
$startDate = '2019-02-12';
$endingDate = '2019-05-31';

$res = $mysqli->query("SELECT * FROM customer As c
INNER JOIN orders As o ON c.id=o.customer_id AND order.new_date BETWEEN '{$startDate}' AND '{$endingDate}' 
ORDER BY new_date ASC");

while ($row = $res->fetch_assoc()):
?>
     <tr>
          <td><?php echo $row['id']; ?></td>
          <td><?php echo $row['customer_name']; ?></td>
          <td><?php echo $row['customer_address']; ?></td>
          <td><?php echo $row['status']; ?></td>
     </tr>
<?php
endwhile;
?>
</tbody>
</table>
<?php
include '../Assets/footer.php';
?>