<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>

<a href="Oindex.php" class="btn btn-success btn-md"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</a>

<?php
if (isset($_POST['bts'])):
    if ($_POST['oid'] != null && $_POST['sts'] != null && $_POST['oc'] != null) {
        $stmt = $mysqli->prepare('INSERT INTO orders (customer_id,status,order_code) VALUES (?,?,?)');
        $stmt->bind_param('iss', $oid, $sts, $oc);
        $cid = $_POST['oid'];
        $sts = $_POST['sts'];
        $oc = $_POST['oc'];
        if ($stmt->execute()):
?>
		<p></p>
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
		  <strong>Berhasil!</strong> Silahkan tambah lagi, jika ingin keluar klik <a href="Oindex.php">Home</a>.
		</div>
		<?php
    else:
    ?>

<p></p>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <strong>Error!</strong> Inserting data!!!.<?php echo $stmt->error; ?>
</div>
<?php
endif;
    } else {
        ?>
<p></p>
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <strong>Gagal!</strong> Form tidak boleh kosong, tolong diisi.
</div>
<?php
    }
endif;
?>

 <p>
</p>
     <div class="panel panel-default">
     <div class="panel-body">

  <form role="form" method="post">
  <div class="form-group">
      <label for="oid">Order Id</label>
      <input type="text" class="form-control" name="oid" id="oid" placeholder="Order Id">
    </div>

    <div class="form-group">
        <div class="radio">
        <label for="sts"><input type="radio" name="sts" value="1" checked>Paid</label>
        </div>
        <div class="radio">
        <label for="sts"><input type="radio" name="sts" value="0">Unpaid</label>
        </div>
    </div>

    <div class="form-group">
      <label for="oc">Ordercode</label>
      <input type="text" class="form-control" name="oc" id="oc" placeholder="Order Code">
    </div>
    <button type="submit" name="bts" class="btn btn-default">Submit</button>
  </form>
   <div class="form-group">
      <label for="oc">Date</label>
      <input type="datetime-local" class="form-control" name="dt" id="dt" placeholder="Date">
    </div>
    <button type="submit" name="bts" class="btn btn-default">Submit</button>
  </form>
<?php
include '../Assets/footer.php';
?>