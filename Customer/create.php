<?php
  include '../Assets/config.php';
  include '../Assets/header.php';
?>
<a href="index.php" class="btn btn-success btn-md"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</a>

<?php
if (isset($_POST['bts'])):
    if ($_POST['nm'] != null && $_POST['ar'] != null) {
        $stmt = $mysqli->prepare('INSERT INTO customer (customer_name,customer_address) VALUES (?,?)');
        $stmt->bind_param('ss', $nm, $ar);
        $nm = $_POST['nm'];
        $ar = $_POST['ar'];
        if ($stmt->execute()):
 ?>
	<p></p>
	<div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>Successfull!</strong> Data successfully inserted <a href="index.php">Home</a>.
	</div>
	<?php
  else:
  ?>
<p></p>
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <strong>Fail!</strong> Error Inserting!!!.<?php echo $stmt->error; ?>
</div>
<?php
endif;
    } else {
?>
<p></p>
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <strong>Error!</strong> Form tidak boleh kosong, tolong diisi.
</div>
<?php
    }
endif;
?>

     <p>
</p>
     <div class="panel panel-default">
       <div class="panel-body">

  <form role="form" method="post">
    <div class="form-group">
      <label for="nm">Name</label>
      <input type="text" class="form-control" name="nm" id="nm" placeholder="Enter Name">
    </div>
    
    <div class="form-group">
      <label for="ar">Address</label>
      <textarea class="form-control" name="ar" id="ar" rows="3"></textarea>
    </div>
    <button type="submit" name="bts" class="btn btn-default">Submit</button>
  </form>
<?php
include '../Assets/footer.php';
?>