<?php
include '../Assets/config.php';
include '../Assets/header.php';
?>
<p>
     <a href="create.php" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Data</a>
</p>

<table id="ghatable" class="display table table-bordered table-stripe" cellspacing="0" width="100%">
<thead>
     <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Address</th>
          <th>Created-Date</th>
          <th>Updated-Date</th>
          <th>Action</th>
     </tr>
</thead>
<tbody>

<?php
$res = $mysqli->query('SELECT * FROM customer');
while ($row = $res->fetch_assoc()):
?>
     <tr>
          <td><?php echo $row['id']; ?></td>
          <td><?php echo $row['customer_name']; ?></td>
          <td><?php echo $row['customer_address']; ?></td>
          <td><?php echo $row['created_date']; ?></td>
          <td><?php echo $row['modified_date']; ?></td>
          <td>
          <a href="update.php?u=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
          <a onclick="return confirm('Are you want deleting data')" href="delete.php?d=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
          </td>
     </tr>
<?php
endwhile;
?>
</tbody>
</table>
<?php
include '../Assets/footer.php';
?>